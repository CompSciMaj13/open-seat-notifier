#!/usr/bin/env python3
import argparse
import os
import time

from checkers.cbc_checker import CBCClassChecker

BASE_DIR = os.path.dirname(os.path.relpath(__file__))
PW_FILENAME = BASE_DIR + "password.txt"
CHECK_SLEEP = 600

def main() -> None:
    args = parse_args()
    checker = CBCClassChecker(args.server_email, args.to_addrs, args.pass_file)
    while True:
        output = checker.check(args.name, args.item_num, args.quarter, args.year,
                               email_on=args.email_on)
        print(output)
        time.sleep(args.sleep)

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--name", help="Name of the class", type=str, required=True)
    parser.add_argument("-i", "--item_num", help="Class item number", type=int, required=True)
    parser.add_argument("-q", "--quarter", help="Class quarter", type=str,
                        choices=["Spring", "Summer", "Fall", "Winter"], required=True)
    parser.add_argument("-y", "--year", help="Class year", type=int, required=True)

    parser.add_argument("--email-on", help="Send an email when class is either open or closed",
                        type=str, choices=["open", "closed"], required=False)
    parser.add_argument("--sleep", help="Amount of time to wait between checking class status",
                        type=int, required=False, default=CHECK_SLEEP)
    parser.add_argument("--server-email", help="Email to log in and send from", type=str,
                        required=False)
    parser.add_argument("--to-addrs", help="Email addresses to send to. Example --to-addrs " \
                        "test1@email.com test2@email.com test3@email.com", nargs="+",
                        required=False, default=[""])
    parser.add_argument("--pass-file", help="Password file for email. Default is in the same " \
                        "directory of script named 'password.txt'", required=False,
                        default=PW_FILENAME)
    return parser.parse_args()

if __name__ == "__main__":
    main()
