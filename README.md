# CBC Open Seat Notifier
This is a simple project that utilizes selenium and Firefoxs headless browser to query the Columbia Basin College class schedule website and report if the class status is open or closed. An email can also be sent to notify of the class status.

## Requirements

* Firefox browser
* python3
* pip3

## Installation

1. Git clone project:  
    ```git clone git@gitlab.com:CompSciMaj13/open-seat-notifier.git```
1. Change directory into the project `open-seat-notifier`
1. Create a directory for a python virtual environment:  
    ```mkdir venv```
1. Create a python virtual environment:  
    ```python3 -m venv venv/```
1. Activate the virtual environment:  
    ```source venv/bin/activate```
1. Install required python libraries with pip:  
    ```pip3 install -r requirements.txt```

## Usage

The `open_seat_notifier.py` script is very easy to execute. There are 4 required pieces of information to successfully check the status of a class. The class name, the classes item number, quarter and year. Here is an example:

```bash
./open_seat_notifier.py --name "AMT 107" --item_num 6310 --quarter Fall --year 2018

# or with the shorthand flags
./open_seat_notifier.py -n "AMT 107" -i 6310 -q Fall -y 2018
```

The output will look similar to this:

```bash
Class 6310 AMT 107 is CLOSED - Tue Oct 16 00:12:06 2018
```

By default, the sleep interval between queries is 600 seconds or 10 minutes. This can be modified by passing the `--sleep` flag. Continuing with our previous example, this is how to have a 5 minute sleep between intervals.

```bash
./open_seat_notifier.py -n "AMT 107" -i 6310 -q Fall -y 2018 --sleep 300
```

> **Caution:** Please be respectful to any website called with selenium and shell scripts. It is your responsibility to use this script appropriately. The user of this script assumes full responsibility for any damages caused by this script locally or remote.

### Email Notifications

Gmail is the only email server supported as that is what I've used for writing this script. Feel free to modify and add support for your own email server.

I'll walk you through how to configure sending email notifications for the status of a class.

#### password.txt

The recommended way to authenticate to the gmail server is by creating an app password for this script. This makes it easy to revoke or expire the app password and prevent needing to change the master account password.

1. Navigate to [https://security.google.com/settings/security/apppasswords](https://security.google.com/settings/security/apppasswords)
1. Create the app password and copy it.
1. Create a text document in the projects folder named `password.txt` or any other name you'd like.\
1. Paste the Gmail app password into the document. Ensure there are no extra text, lines or spaces in the `password.txt` document.

#### Sending
After the `password.txt` file has been created, you are ready to setup email notifications. Just pass `--email-on`, `--server-email`, and `--to-addrs`. The options for the `--email-on` flag are either `open` or `closed` depending on if you want to send an email notification if the class status is OPEN or CLOSED. 

If you named the password file other than the default or have it stored in a different location other than the projects root directory, you will also pass the `--pass-file` flag to specify its name/location.

Here's what an example would look like to send an email when class AMT 107 has an OPEN status:

```bash
./open_seat_notifier.py -n "AMT 107" -i 6310 -q Fall -y 2018 --email-on open --server-email my-email@email.com --to-addrs john.doe@email.com jane.doe@email.com
```

And the output will be similar to the following:

```text
Class 6310 AMT 107 is OPEN - Tue Oct 16 00:48:40 2018

Content-Type: multipart/mixed; boundary="================="
MIME-Version: 1.0
From: my-email@email.com
To: john.doe@email.com, jane.doe@email.com
Subject: Class 6310 AMT 107 is OPEN - Tue Oct 16 00:48:40 2018

--=================
Content-Type: text/plain; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit

Class 6310 AMT 107 is OPEN - Tue Oct 16 00:48:40 2018

http://www.columbiabasin.edu/apps/cs/default.aspx
--=================--
```

If there are any questions or concers, please feel free to create an issue, or email me.
