"""Columbia Basin College checker module"""
from typing import List

import os
import smtplib
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class BaseWebDriver:
    """Base class WebDriver"""
    def __init__(self) -> None:
        self.driver = None # type: webdriver

    def __enter__(self) -> webdriver:
        return self.driver

    def __exit__(self, *exc_info) -> None:
        self.driver.quit()


class FFWebDriver(BaseWebDriver):
    """Firefox webdriver class"""
    def __init__(self) -> None:
        super().__init__()
        options = webdriver.FirefoxOptions()
        options.set_headless(True)
        self.driver = webdriver.Firefox(firefox_options=options)
        self.driver.implicitly_wait(30)


class CBCClassChecker:
    """Module to know whether a CBC class is open for enrollment

    :param server_email: Email to send email from
    :type server_email: str
    :param to_addrs: Addresses to send emails to
    :type to_addrs: List[str]
    :param pw_file: Path to password file for email authentication
    :type pw_file: str
    """
    def __init__(self, server_email: str = None, to_addrs: List[str] = None, pw_file: str = None) -> None:
        self.search_url = "http://www.columbiabasin.edu/apps/cs/default.aspx"

        self.server_email = server_email
        self.to_addrs = to_addrs
        self.pw_file = pw_file
        if server_email and pw_file:
            server = self.__get_server()
            server.quit()

    def check(self, name: str, item_num: int, quarter: str, year: int, email_on: str = None) -> str:
        """Query CBC website for the status of the class and return the status.

        :param name: Name of class
        :type name: str
        :param item_num: Item number of class
        :type item_num: int
        :param quarter: Quarter of the year the class is offered, e.g. Fall, Spring, etc
        :type quarter: str
        :param year: Year the class is offered
        :type year: int
        :param email_on: Specify to email when status is "open" or "closed"
        :type email_on: str
        """
        localtime = time.asctime(time.localtime(time.time()))
        status = self.__get_status(item_num, quarter, year)
        message = f"Class {item_num} {name} is {status} - {localtime}"
        email_msg = ""
        if email_on:
            if email_on.upper() in ["OPEN", "CLOSED"]:
                if status == email_on.upper():
                    email_msg = "\n\n" + self.__send_email(message)
            else:
                print("email_on must be specified as 'open', or 'closed'. No email sent.")
        return message + email_msg

    def __get_status(self, item_num: int, class_quarter: str, class_year: int) -> str:
        with FFWebDriver() as driver:
            driver.get(self.search_url)
            driver.find_element_by_id("ctl00_plhContent_lstYRQ").send_keys("{} {}".format(class_quarter, class_year))
            driver.find_element_by_id("ctl00_plhContent_txtItem").send_keys(item_num)
            driver.find_element_by_id("ctl00_plhContent_btnSearch").click()
            try:
                status = driver.find_element_by_id("ctl00_plhContent_gvSchedule_ctl02_lblStatus").text
            except NoSuchElementException:
                print("There are no classes for that search criteria.")
                exit(1)
            return status

    def __send_email(self, message: str) -> str:
        server = self.__get_server()
        msg = self.__generate_msg(message)
        try:
            if self.server_email and self.to_addrs:
                server.sendmail(self.server_email, self.to_addrs, msg)
                server.quit()
                return msg
            print("Could not send email! Server email address and/or to addresses not specified.")
        except smtplib.SMTPRecipientsRefused as err:
            print("The following recipients were refused by the server. Please try correct and try again!")
            print(err.recipients)
        except smtplib.SMTPSenderRefused as err:
            print("The server didn't accept the from address {}!".format(err.sender))
            print(err)
        except smtplib.SMTPException as err:
            print("Error while attempting to send email!")
            print(err)
        return ""

    def __get_server(self) -> smtplib.SMTP:
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.starttls()
            if self.server_email:
                server.login(self.server_email, self.__get_password())
            else:
                print("Server email address not specified! Could not log on to SMTP server")
                exit(1)
            return server
        except smtplib.SMTPConnectError:
            print("Could not connect to the email server! Please check the network connection and try again.")
        except smtplib.SMTPAuthenticationError:
            print("Error while authenticating with email server! Please check credentials and try again.")
        except smtplib.SMTPNotSupportedError as err:
            print("Server does not appear to support SMTP! Please check SMTP support and try again.")
            print(err)
        except smtplib.SMTPException as err:
            print("Error while attempting to log on to the email server!")
            print(err)
        exit(1)

    def __get_password(self) -> str:
        password = ""
        try:
            if self.pw_file:
                with open(self.pw_file, "r") as passfile:
                    password = passfile.readline()
            else:
                print("No password filename specified!")
                exit(1)
        except FileNotFoundError:
            print("Password file not found!")
            exit(1)
        return password

    def __generate_msg(self, message: str) -> str:
        msg = MIMEMultipart()
        if self.server_email and self.to_addrs:
            msg["From"] = self.server_email
            msg["To"] = ", ".join(self.to_addrs)
        else:
            print("Server email address and to addresses are required to send message!")
        msg["Subject"] = message
        msg.attach(MIMEText(message + "\n\n" + self.search_url, "plain"))
        return msg.as_string()
